@ECHO OFF
:: Check for Python Installation
py --version 2>NUL
if errorlevel 1 goto errorNoPython


py "%CD%\src\data_upload.py" %__CD__:~,-1%
echo "Files have loaded successfully!"
:: Reaching here means Python is installed.
:: Execute stuff...

:: Once done, exit the batch file -- skips executing the errorNoPython section
goto:eof

:errorNoPython
echo.
echo Error^: Python not installed
powershell -ExecutionPolicy Bypass -Command "(New-Object Net.WebClient).DownloadFile('https://www.python.org/ftp/python/3.9.0/python-3.9.0-amd64.exe', 'python-3.9.0-amd64.exe')" & PAUSE && python-3.9.0-amd64.exe & del python-3.9.0-amd64.exe & echo "Run file again."
