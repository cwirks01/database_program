import os
import re
import sys

from time import gmtime, strftime

RFI_num = input("What is the RFI Number? (e.g. RFI 00027, Enter 27) ")

try:
    ROOT=sys.argv[1]
except Exception as e:
    ROOT="C:\\data\\"
    os.makedirs(ROOT)
    print(e)

PARENT_FOLDER=os.path.basename(ROOT)
TIMENOW=strftime("%d%m%Y",gmtime())
ARCHIVE=os.path.join(ROOT,"archive")
bucket_root='s3://gost-internal-upload'

if __name__=='__main__':
    if not os.path.exists(ARCHIVE):
        os.makedirs(ARCHIVE)

    for path, dir, files in os.walk(ROOT):
        for file in files:
            # Find files that end in csv and that are not in the archive directory
            if file.endswith(".csv") and not re.search("archive",path) and not re.search("src",path):
                try:
                    filename = os.path.splitext(file)[0]
                    
                    # Renaming file wit rfi number and date
                    new_name=f"{filename}_RFI{RFI_num}_{TIMENOW}.csv"
                    
                    # get subfolders to cobwebs
                    cobwebs_folder_basename=path.split(f"{PARENT_FOLDER}\\")[-1]
                    
                    # Appending subfolders to the archive folder if exists
                    if len(path.split(f"{PARENT_FOLDER}\\")) > 1 and not cobwebs_folder_basename=='':
                        new_dir_path=os.path.join(ARCHIVE,cobwebs_folder_basename)
                        bucket_basename="/"+cobwebs_folder_basename.replace('\\','/')+"/"
                    else:
                        new_dir_path=os.path.join(ARCHIVE)
                        bucket_basename=''
                    
                    # Create subfolders in archive if they do not exist
                    if not os.path.exists(new_dir_path):
                        os.makedirs(new_dir_path)
            
                    new_path=os.path.join(new_dir_path,new_name)
                    
                    # Reanaming the file and sending to archive
                    os.rename(os.path.join(path,file),new_path)
                    
                    print(os.path.join(path,new_name))
                    aws_cmd=f"""aws s3 cp "{new_path}" "{bucket_root}{bucket_basename}" """
                    os.system(aws_cmd)
                                   
                except Exception as e:
                    print(e)                
    