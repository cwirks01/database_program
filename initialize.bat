@ECHO OFF

:: Checking if aws cli is installed. if not then program will install AWSCLIV2.msi then start initial setup

if %~n0%~x0 == re-initialize.bat (goto:re-initialize.bat) else (goto:initialize_folder)

:initialize_folder
cls & color 0B
Mode con cols=90 lines=5
set Location=%ProgramFiles%\Amazon\AWSCLIV2
set FileName=aws.exe
echo( & cls
echo(  & echo  Please Wait for moment .... Searching for "%FileName%" on "%Location%"
TimeOut /T 3 /NoBreak>Nul
cls
PAUSE

IF EXIST "%Location%\%FileName%" (color 0A && echo The "%FileName%" is installed & Start "" "%CD%\src\initial_setup.bat" 
) ELSE (
    Color 0C & echo The "%FileName%" is not installed & pause & powershell -Command "(New-Object Net.WebClient).DownloadFile('https://awscli.amazonaws.com/AWSCLIV2.msi', 'AWSCLIV2.msi')" & msiexec.exe /i "AWSCLIV2.msi" && del "AWSCLIV2.msi" && ECHO "Reinitiate batch file" && exit
)

:: Creating a shortcut file of the run bat command in main folder
set SCRIPT="%TEMP%\%RANDOM%-%RANDOM%-%RANDOM%-%RANDOM%.vbs"

echo Set oWS = WScript.CreateObject("WScript.Shell") >> %SCRIPT%
echo sLinkFile = "%CD%\run.lnk" >> %SCRIPT%
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%
echo oLink.TargetPath = "%CD%\src\run.bat" >> %SCRIPT%
echo oLink.IconLocation = "%CD%\src\gost_logo.ico" >> %SCRIPT%
echo oLink.Save >> %SCRIPT%

cscript /nologo %SCRIPT%
del %SCRIPT%

:: Moving initialize.bat to src file and renaming to initialize_complete.bat
goto:eof

:re-initialize.bat
IF EXIST "%~dp0..\cobwebs" (RMDIR /S /Q %~dp0..\cobwebs & PAUSE) ELSE (ECHO syncing folders & PAUSE)

cls & color 0B
Mode con cols=90 lines=5
set Location=%ProgramFiles%\Amazon\AWSCLIV2
set FileName=aws.exe
echo( & cls
echo(  & echo  Please Wait for moment .... Searching for "%FileName%" on "%Location%"
TimeOut /T 3 /NoBreak>Nul
cls
PAUSE

IF EXIST "%Location%\%FileName%" (color 0A && echo The "%FileName%" is installed & PAUSE & cmd /c ""%CD%\initial_setup.bat""
) ELSE (
    Color 0C & echo The "%FileName%" is not installed & pause & powershell -Command "(New-Object Net.WebClient).DownloadFile('https://awscli.amazonaws.com/AWSCLIV2.msi', 'AWSCLIV2.msi')" & PAUSE & msiexec.exe /i "AWSCLIV2.msi" && del "AWSCLIV2.msi" && ECHO "Reinitiate batch file" && exit
)
