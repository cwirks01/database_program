# **README** #

Follow the below steps to initialize and configure AWS CLI and sync to the AWS S3 "cobwebs" bucket.

## Setup ##

1. Unzip file with installer and source files

2. Run ***initialize.bat*** file with credentials and keys. The batch file will confirm if AWS CLI is already installed. Once installed you will be prompted to enter you AWS credentials 

3. Once initialize is complete, there will be a run file and new folders "adid", "data_drop", "social-media. There will also be a run link with the GOST icon. 

4. Place csv's to be copied into their respective folder. 

5. Execute the run link with GOST icon and input the RFI number when prompted.

## AWS CLI Installation ##

1. Download and run the AWS CLI MSI installer for Windows (64-bit):

    [**AWS CLI**](https://awscli.amazonaws.com/AWSCLIV2.msi)

2. To confirm the installation, open the Start menu, search for cmd to open a command prompt window, and at the command prompt use the ***aws --configure*** command.

## AWS Configure ##

1. For general use, the ***aws configure*** command is the fastest way to set up your AWS CLI installation. When you enter this command, the AWS CLI prompts you for four pieces of information:

   * AWS Access Key ID [None]: (***Your AWS Access Key ID***)
   * AWS Secret Access Key [None]: (***Your Secret Access Key***)
   * Default region name [None]: us-east-1
   * Default output format [None]: json

2. You can test your connection with the following command: 
***aws s3 ls***. This will list all the buckets in GOST S3 in cmd.